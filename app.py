# import
from flask import Flask,render_template,request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_fontawesome import FontAwesome
from flask_migrate import Migrate

import os
# from datetime import datetime

#instantiate
app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:pass@localhost:5432/olipedia'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Audio(db.Model):
    _tablename_ = 'Audio'
    id = db.Column(db.Integer,primary_key=True)
    audio_name = db.Column(db.String,unique=True)
    audio_url = db.Column(db.String)
    image_url = db.Column(db.String)
    author_name = db.Column(db.String)
    google_podcast_URL = db.Column(db.String)
    apple_podcast_URL = db.Column(db.String)
    spotify_link = db.Column(db.String)
    youtube_link = db.Column(db.String)
    audio_timing = db.Column(db.String)
def __init__(self,id,audio_name,audio_url,author_name,google_podcast_URL,apple_podcast_URL,spotify_link,youtube_link,image_url,audio_timing):
    self.id = id
    self.audio_name = audio_name
    self.audio_url = audio_url
    self.author_name = author_name
    self.google_podcast_URL = google_podcast_URL
    self.apple_podcast_URL = apple_podcast_URL
    self.spotify_link = spotify_link
    self.youtube_link = youtube_link
    self.image_url = image_url
    self.audio_timing = audio_timing

@app.route('/')
def hello_world():
    return 'Hello Welcome to Flask'

@app.route('/audio', methods=['POST','GET'])
def audio():
    if request.method == 'POST':
         audio_name = request.form['audio_name']
         audio_url = request.form['audio_url']
         image_url = request.form['img_url']
         author_name = request.form['author_name']
         google_podcast_url = request.form['google_podcast_url']
         apple_podcast_url = request.form['apple_podcast_url']
         spotify_url = request.form['spotify_url']
         youtube_link = request.form['youtube_link']
         audio_timing = request.form['audio_timing']
         new_audio = Audio(audio_name=audio_name, audio_url=audio_url, image_url=image_url, author_name=author_name, google_podcast_URL=google_podcast_url, apple_podcast_URL=apple_podcast_url, spotify_link=spotify_url, youtube_link=youtube_link, audio_timing=audio_timing)         
         
         db.session.add(new_audio)
         db.session.commit()
         return redirect('/') 
         
    else:
        return render_template('audio.html')


    

@app.route('/view')
def view():
    audios = Audio.query.all()
    for audio in audios:
        print(audio.audio_name)
        print(audio.audio_url)
        print(audio.author_name)
    return "0"
@app.route('/home/<int:page_num>', methods=['POST','GET'])
def home(page_num):
     if request.method == 'GET':
        audios = Audio.query.paginate(per_page=6, page=page_num, error_out=True)
        return render_template('index.html', title='Olipedia', username='Athithya', query=audios)

def readMore():
    print("Moving Forward...")
    dots = document.getElementById("dots")
    moreText = document.getElementById("more")
    btnText = document.getElementById("myBtn")
    if dots.style.display == 'none':
        dots.style.display = "inline"
        btnText.innerHTML = "Read more"
        moreText.style.display = "none"
    else:
        dots.style.display = "none"
        btnText.innerHTML = "Read less"
        moreText.style.display = "inline"
  
@app.route('/user/<username>')
def show_user(username):
    return render_template('index.html',title="Olipedia",username=username)

if __name__ == '__main__':
    if not os.path.exists('olipedia'):
        db.create_all()
    app.run(host='0.0.0.0',port='5000',debug=True)

