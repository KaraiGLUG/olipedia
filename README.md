# Olipedia
ஒலிபீடியாவில் உரை கோப்புகள் பொதுவுடமை புத்தகங்கள். ஒலி உருவாக்கம் செய்யப்பட்டு, இங்கு பதிவேற்றம் செய்யப்பட்டுள்ளது. அதை நீங்கள் கேட்டு மகிழலாம்.

#set up environment:
-----------------------
$ python3 -m venv env

$ source env/bin/activate

$ apt install python3-pip

$ pip3 install -r requirements.txt


#Close the environment:
----------------------
$ deactivate 


#Run application:
------------------
$ source env/bin/activate

$ python app.py


application runs at http://0.0.0.0:/5000